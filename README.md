## Installing k8 on Digitalocean

read more : https://www.digitalocean.com/community/tutorials/how-to-create-a-kubernetes-1-11-cluster-using-kubeadm-on-ubuntu-18-04

- Start ubuntu 18.04 LTS vps
- Setup ssh for accessing vps servers
- Install ansible locally
- Change IPs of master and workers on host file

#### Create non-root users

```sh
$ ansible-playbook -i hosts initial.yml
```

#### Install k8 dependencies

```sh
$ ansible-playbook -i hosts kube-dependencies.yml
```

#### Setting Up the Master Node

```sh
$ ansible-playbook -i hosts master.yml
```

#### Setting Up the Worker Nodes

```sh
$ ansible-playbook -i hosts workers.yml
```

Setup is complete! check in master for kubectl commands
